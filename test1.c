#include <linux/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

// A sample userapp. Adapt the code to be used for reverse lseek for using reverse and out of bound checking.

#define CDRV_IOC_MAGIC 'Z'
#define E2_IOCMODE1 _IOWR(CDRV_IOC_MAGIC, 1, int)
#define E2_IOCMODE2 _IOWR(CDRV_IOC_MAGIC, 2, int)


int f0, f1, f2;
int numOfThreads = 2;
int rc;
int dir = 0;
pthread_t threads[2];

void *test(void *arg)
{
    int size = 20;
	char buf[size];
    int id = (int)arg;

    memset(buf, 0, size);

    if (id == 0) {
    
    rc = ioctl(f0, E2_IOCMODE2, dir);
    if (rc == -1)
        	{ perror("\n***error in ioctl***\n");
        	  return -1;}
    }
	

    if (id == 1) {
    read(f1,buf,size);	
    }
	return 0;
}

int main(int argc, char *argv[]) {

	int i;
	char buf[1024] = {0};
	char buf2[1024] = {0};
    	//int dir = 0;

    	f0 = open("/dev/a5", O_RDWR);
    	if (f0 < 0) { 
		perror("Error while opening a5");
    	  	return -1;
	}
    	f1 = open("/dev/a5", O_RDWR);
    	if (f1 < 0) { 
		perror("Error while opening a5");
    		close(f0);
    		return -1;

	}
        
	for (i = 0; i < numOfThreads; i++)
            pthread_create(&(threads[i]), NULL, test, (void*)i);

        for (i=0; i<nTHread; i++)
            pthread_join(threads[i], NULL);

	close(f0);
	close(f1);

	printf("Successfully executed T1 and T2 without deadlocks");
	
	return 0;
}
